﻿
namespace DesignPatterns_Facade
{
    public interface IFacade
    {
        void DoWork();
    }
}
