﻿
namespace DesignPatterns_Facade
{
    public class Facade : IFacade
    {
        private readonly SubSystemOne _subSystemOne;
        private readonly SubSystemTwo _subSystemTwo;
        private readonly SubSystemThree _subSystemThree;

        public Facade(SubSystemOne subOne, SubSystemTwo subTwo, SubSystemThree subThree)
        {
            _subSystemOne = subOne;
            _subSystemTwo = subTwo;
            _subSystemThree = subThree;
        }

        public void DoWork()
        {
            _subSystemOne.SubSystemOneMethod();
            _subSystemTwo.SubSystemTwoMethod();
            _subSystemThree.SubSystemThreeMethod();
        }
    }
}
