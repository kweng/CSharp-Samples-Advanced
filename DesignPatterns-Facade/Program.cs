﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace DesignPatterns_Facade
{
    /// <summary>
    /// A facade is an object that serves as a front-facing interface,
    /// masking more complex underlying or structural code.
    /// </summary>
    class Program
    {
        public static IServiceProvider BuildServiceProvider(IServiceCollection services)
        {
            services.AddSingleton<SubSystemOne>();
            services.AddSingleton<SubSystemTwo>();
            services.AddSingleton<SubSystemThree>();

            services.AddScoped<IFacade, Facade>();

            return services.BuildServiceProvider();
        }

        static void Main()
        {
            IServiceCollection services = new ServiceCollection();
            IServiceProvider serviceProvider = BuildServiceProvider(services);

            // Uniform interface to a large subsystem of classes.
            var facade = serviceProvider.GetService<IFacade>();
            facade.DoWork();
        }
    } 
}
