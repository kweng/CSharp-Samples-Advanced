﻿using System;

namespace DesignPatterns_Facade
{
    public class SubSystemOne
    {
        public void SubSystemOneMethod()
        {
            Console.WriteLine("SubSystemOne Logic!");
        }
    }
}
