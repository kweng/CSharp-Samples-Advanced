﻿using System;

namespace DesignPatterns_Facade
{
    public class SubSystemThree
    {
        public void SubSystemThreeMethod()
        {
            Console.WriteLine("SubSystemThree Logic!");
        }
    }
}
