﻿using System;

namespace DesignPatterns_Facade
{
    public class SubSystemTwo
    {
        public void SubSystemTwoMethod()
        {
            Console.WriteLine("SubSystemTwo Logic!");
        }
    }
}
