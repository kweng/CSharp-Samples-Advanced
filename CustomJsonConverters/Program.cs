﻿using CustomJsonConverters.Serialization;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json.Serialization;

IServiceCollection services = new ServiceCollection();
IServiceProvider serviceProvider = BuildServiceProvider(services);

IServiceProvider BuildServiceProvider(IServiceCollection services)
{
    services.AddSingleton<ISerializer, SystemTextJsonSerializer>();
    return services.BuildServiceProvider();
}

var serializer = (ISerializer)serviceProvider.GetRequiredService(typeof(ISerializer));
var jsonString = GetJsonString();
var result = serializer.Deserialize<Values>(jsonString);


string GetJsonString()
    => "{\"BoolValue\":1}";

internal record Values
{
    [JsonConverter(typeof(BoolConverter))]
    public bool BoolValue { get; set; }
}
