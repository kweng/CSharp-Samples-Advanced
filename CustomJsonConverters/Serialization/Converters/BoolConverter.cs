﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace CustomJsonConverters.Serialization;

internal class BoolConverter : JsonConverter<bool>
{
    public override bool Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return reader.TokenType switch
        {
            JsonTokenType.True => true,
            JsonTokenType.False => false,

            JsonTokenType.String => bool.TryParse(reader.GetString(), out var convertedToBool)
                ? convertedToBool
                : throw new JsonException(),

            JsonTokenType.Number => reader.TryGetInt32(out int jsonValue)
                ? Convert.ToBoolean(jsonValue)
                : false,

            _ => throw new JsonException(),
        };
    }

    public override void Write(Utf8JsonWriter writer, bool value, JsonSerializerOptions options)
    {
        writer.WriteBooleanValue(value);
    }
}
