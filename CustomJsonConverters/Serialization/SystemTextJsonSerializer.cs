﻿using System.Text.Json;

namespace CustomJsonConverters.Serialization;

internal sealed class SystemTextJsonSerializer : ISerializer
{
    private static readonly JsonSerializerOptions jsonSerializerOptions = new()
    {
        PropertyNameCaseInsensitive = true,
        Converters = 
        { 
            new BoolConverter() 
        }
    };

    public string Serialize<T>(T value) where T : class
        => JsonSerializer.Serialize<T>(value, jsonSerializerOptions);

    public T? Deserialize<T>(string value) where T : class
        => JsonSerializer.Deserialize<T>(value, jsonSerializerOptions);
}
