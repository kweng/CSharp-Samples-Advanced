﻿using DesignPatterns_Factory.Factory.Providers;
using DesignPatterns_Factory.Factory.Utils;

namespace DesignPatterns_Factory.Factory
{
    public interface IHttpRequestFactory
    {
        IHttpRequest CreateHttpRequest(RequestMethodType methodType);
    }
}
