﻿using DesignPatterns_Factory.Factory.Providers;
using DesignPatterns_Factory.Factory.Utils;
using System;
using System.Collections.Generic;

namespace DesignPatterns_Factory.Factory
{
    public abstract class BaseFactory
    {
        protected IDictionary<RequestMethodType, Func<IHttpRequest>> Requests = CreateRequestMap();

        private static IDictionary<RequestMethodType, Func<IHttpRequest>> CreateRequestMap()
        {
            return new Dictionary<RequestMethodType, Func<IHttpRequest>>()
            {
                { RequestMethodType.GET, () => new HttpGetRequest() },
                { RequestMethodType.POST, () => new HttpPostRequest() },
            };
        }
    }
}
