﻿using DesignPatterns_Factory.Factory.Providers;
using DesignPatterns_Factory.Factory.Utils;

namespace DesignPatterns_Factory.Factory
{
    public class HttpRequestFactory : BaseFactory, IHttpRequestFactory
    {
        public IHttpRequest CreateHttpRequest(RequestMethodType methodType)
        {
            Requests.TryGetValue(methodType, out var requestProvider);
            if (requestProvider is null)
                throw new System.NotSupportedException("No provider found!");

            return requestProvider();
        }
    }
}
