﻿
namespace DesignPatterns_Factory.Factory.Providers
{
    public interface IHttpPostRequest : IHttpGetRequest
    {
        string Body { get; set; }
    }
}
