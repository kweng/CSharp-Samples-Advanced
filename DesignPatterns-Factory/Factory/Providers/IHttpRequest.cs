﻿using System.Collections.Generic;

namespace DesignPatterns_Factory.Factory.Providers
{
    public interface IHttpRequest
    {
        void Send(string Url, IDictionary<string, string> Headers, string body = default);
    }
}
