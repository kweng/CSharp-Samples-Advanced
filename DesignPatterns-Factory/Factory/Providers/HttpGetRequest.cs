﻿using System.Collections.Generic;

namespace DesignPatterns_Factory.Factory.Providers
{
    public class HttpGetRequest : IHttpGetRequest
    {
        public IDictionary<string, string> Headers { get; set; }
        public string Url { get; set; }

        public void Send(string Url, IDictionary<string, string> Headers, string body = default)
        {
            throw new System.NotImplementedException();
        }
    }
}
