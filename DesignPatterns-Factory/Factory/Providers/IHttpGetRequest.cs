﻿using System.Collections.Generic;

namespace DesignPatterns_Factory.Factory.Providers
{
    public interface IHttpGetRequest : IHttpRequest
    {
        string Url { get; set; }

        IDictionary<string, string> Headers { get; set; }
    }
}
