﻿
namespace DesignPatterns_Factory.Factory.Utils
{
    public enum RequestMethodType
    {
        GET,
        POST
    }
}
