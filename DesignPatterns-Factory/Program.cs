﻿using DesignPatterns_Factory.Factory;
using DesignPatterns_Factory.Factory.Utils;

namespace DesignPatterns_Factory
{
    class Program
    {
        static void Main()
        {
            IHttpRequestFactory httpFactory = new HttpRequestFactory();
            var request = httpFactory.CreateHttpRequest(RequestMethodType.POST);
            request.Send(null, null, null);
        }
    }
}
