﻿
// install Ardalis.SmartEnum package

using Ardalis.SmartEnum;

#region Simple Smart Enum
internal sealed class Subscriptions : SmartEnum<Subscriptions>
{
    public static readonly Subscriptions Free = new("FREE", 1);
    public static readonly Subscriptions Premium = new("PREMIUM", 2);

    public Subscriptions(string name, int value)
        : base(name, value)
    { 
    }
}
#endregion

#region Smart Enum With Some Logic
internal sealed class SubscriptionsWithDiscount : SmartEnum<SubscriptionsWithDiscount>
{
    public static readonly SubscriptionsWithDiscount Free = new("FREE", 1, .0);
    public static readonly SubscriptionsWithDiscount Premium = new("PREMIUM", 2, .2);

    public double Discount { get; set; }

    public SubscriptionsWithDiscount(string name, int value, double discount)
        : base(name, value)
        =>
        Discount = discount;

    public double CalculatePriceForMonths(short months)
    {
        try
        {
            var subscriptionPlan = SubscriptionPeriod.FromValue(months);
            return subscriptionPlan.Price - (subscriptionPlan.Price * Discount);
        }
        catch (SmartEnumNotFoundException ex)
        {
            Console.WriteLine(ex.Message);
            return 0;
        }
    }

    private sealed class SubscriptionPeriod : SmartEnum<SubscriptionPeriod>
    {
        public static readonly SubscriptionPeriod OneMonth = new("OneMonth", 1, 10);
        public static readonly SubscriptionPeriod SixMonths = new("SixMonths", 6, 50);
        public static readonly SubscriptionPeriod Year = new("Year", 12, 100);

        public double Price { get; set; }

        public SubscriptionPeriod(string name, int value, double price)
            : base(name, value)
            =>
            Price = price;
    }
}
#endregion
