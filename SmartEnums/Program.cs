﻿using System;
using System.Linq;

public class UniqueProduct
{
    public static string FirstUniqueProduct(string[] products)
    {
        string currentProduct;
        for (int index = 0; index < products.Length; index++)
        {
            currentProduct = products[index];
            bool isDuplicate = false;
            for (int inner = 0; inner < products.Length; inner++)
            {
                if (index == inner)
                    continue;

                if (currentProduct == products[inner])
                {
                    isDuplicate = true;
                    break;
                }
            }

            if (!isDuplicate)
                return currentProduct;
        }
        return null;
    }

    public static void Main(string[] args)
    {
        Console.WriteLine(FirstUniqueProduct(new string[] { "Apple", "Apple", "Bag" }));
    }
}