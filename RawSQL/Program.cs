﻿using Microsoft.EntityFrameworkCore;
using RawSQL.DB;
using System.Linq;

namespace RawSQL
{
    class Program
    {
        static void Main()
        {
            ExecuteSqlQuery();
            ExecuteStoredProcedure();
            ExecuteInterpolatedSqlQuery();
        }


        #region FromSqlRaw
        static void ExecuteSqlQuery()
        {
            using (var context = new AppContext())
            {
                var blogs = context.Blogs
                    .FromSqlRaw("SELECT * FROM dbo.Blogs")
                    .ToList();
            }
        }
        #endregion


        #region StoredProcedure
        static void ExecuteStoredProcedure()
        {
            using (var context = new AppContext())
            {
                
                var user = "user";

                var blogs = context.Blogs
                    .FromSqlRaw("EXECUTE dbo.GetMostPopularBlogsForUser {0}", user)
                    .ToList();
                
            }
        }
        #endregion


        #region FromSqlInterpolated
        static void ExecuteInterpolatedSqlQuery()
        {
            using (var context = new AppContext())
            {
                var searchTerm = "Lorem ipsum";

                var blogs = context.Blogs
                    .FromSqlInterpolated($"SELECT * FROM dbo.SearchBlogs({searchTerm})")
                    .AsNoTracking()
                    .ToList();
            }
        }
        #endregion
    }
}
