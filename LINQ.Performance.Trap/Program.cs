﻿// After a LINQ query is created, it is converted to a tree structure.
// The tree structure is then executed against the data source.
// At query execution time, all query expressions (that is, all components of the query) are evaluated,
// including those expressions that are used in result materialization.

// Performance trap:
// LINQ queries are always executed when the query variable is iterated over, not when the query variable is created.

// If we add a WriteLine to see how many times items are iterated we see the result:

var items = Enumerable.Range(0, 10)
    .Select(x =>
    {
        global::System.Console.WriteLine(x);
        return x;
    });

var count = items.Count();
global::System.Console.WriteLine($"Count: {count}");

var positive = items.All(x => x >= 0);
global::System.Console.WriteLine($"All: {positive}");

// Result:
// 0
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
// Count: 10
// 0
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
// All: True

// To fix this problem, if we iterate more than once
// we need to materialize the query

global::System.Console.WriteLine("\n----------------------------------\n");

items = Enumerable.Range(0, 10)
    .Select(x =>
    {
        global::System.Console.WriteLine(x);
        return x;
    })
    .ToList(); // Materialization

count = items.Count();
global::System.Console.WriteLine($"Count: {count}");

positive = items.All(x => x >= 0);
global::System.Console.WriteLine($"All: {positive}");

// Result:
// 0
// 1
// 2
// 3
// 4
// 5
// 6
// 7
// 8
// 9
// Count: 10
// All: True
