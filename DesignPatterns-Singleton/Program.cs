﻿
using System;

namespace DesignPatterns_Singleton
{
    class Program
    {
        static void Main()
        {
            Lazy<int> lazyInt = new(() => 33);
            Console.WriteLine(lazyInt.Value);

            Logger.Instance.Log("Singleton Implemented");
        }
    }
}
