﻿
namespace DesignPatterns_Singleton
{
    public class Logger
    {
        private static Logger _loggerInstance;

        private static object _lock = new();
        private static bool _isCreated = false;

        private Logger()
        {
        }

        public static Logger Instance
        {
            get
            {
                if (!_isCreated)
                {
                    lock (_lock)
                    {
                        _loggerInstance = new Logger();
                        _isCreated = true;
                    }
                }

                return _loggerInstance;
            }
        }

        public void Log(string log)
        {
            System.Console.WriteLine(log);
        }
    }
}
