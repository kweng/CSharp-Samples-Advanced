﻿
static async Task Main(string[] args)
{
    foreach (var x in await GetEnumerableAsTask())
    {
        Console.WriteLine(x);
    }

    await foreach (var x in GetAsyncEnumerable())
    {
        Console.WriteLine(x);
    }
}

/*
    await foreach is translated by the compiler to:
 
    IAsyncEnumerator<int> e = RangeAsync(10, 3).GetAsyncEnumerator();
    try
    {
      while (await e.MoveNextAsync()) 
         Console.Write(e.Current + " ");
    }
    finally 
    { 
        if (e != null) 
            await e.DisposeAsync(); 
    }
*/

// MORE: https://docs.microsoft.com/en-us/archive/msdn-magazine/2019/november/csharp-iterating-with-async-enumerables-in-csharp-8

static async Task<IEnumerable<int>> GetEnumerableAsTask()
{
    var list = new List<int>();
    for (int index = 0; index < 10; index++)
    {
        list.Add(index);
        await Task.Delay(1000);
    }
    return list;
}

static async IAsyncEnumerable<int> GetAsyncEnumerable()
{
    for (int index = 0; index < 10; index++)
    {
        yield return index;
        await Task.Delay(1000);
    }
}